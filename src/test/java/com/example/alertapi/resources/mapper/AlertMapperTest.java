package com.example.alertapi.resources.mapper;

import com.example.alertapi.infrastructure.model.Alert;
import com.example.alertapi.web.model.AlertDto;
import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;

@ExtendWith(MockitoExtension.class)
class AlertMapperTest {

    @InjectMocks AlertMapper alertMapper;

    @Test
    void whenMapFromAlertNull_thenMappingReturnsNull() {
        BDDAssertions.then(alertMapper.mapFrom(null)).isNull();
    }

    @Test
    void whenMapToAlertNull_thenMappingReturnsNull() {
        BDDAssertions.then(alertMapper.mapTo(null)).isNull();
    }

    @Test
    void whenMapFromAlert_thenMappingIsOk() {
        //given
        final Alert alert =  Alert.builder()
                .id(1)
                .userId(1)
                .createdAt("2021-09-12T20:30:28Z")
                .alertName("alerte")
                .minimumPrice(250000)
                .maxPrice(150000)
                .city("NANTES").build();


        //when
        AlertDto result = alertMapper.mapFrom(alert);

        //then
        Assertions.assertEquals(alert.getAlertName(),result.getAlertName());
        Assertions.assertEquals(alert.getId(),result.getId());
        Assertions.assertEquals(alert.getUserId(),result.getUserId());
        Assertions.assertEquals(alert.getCreatedAt(),result.getCreatedAt().toString());
        Assertions.assertEquals(alert.getMinimumPrice(),result.getMinPrice());
        Assertions.assertEquals(alert.getMaxPrice(),result.getMaxPrice());
        Assertions.assertEquals(alert.getCity(),result.getCity());
    }

    @Test
    void whenMapToAlert_thenMappingIsOk() {
        //given

        final AlertDto alert = new AlertDto()
                .id(1)
                .userId(1)
                .createdAt(Instant.parse("2021-09-12T20:30:28Z"))
                .alertName("alerte")
                .minPrice(250000)
                .maxPrice(150000)
                .city("NANTES");

        //when
        Alert result = alertMapper.mapTo(alert);

        //then
        Assertions.assertEquals(alert.getAlertName(),result.getAlertName());
        Assertions.assertEquals(alert.getId(),result.getId());
        Assertions.assertEquals(alert.getUserId(),result.getUserId());
        Assertions.assertEquals(alert.getCreatedAt().toString(),result.getCreatedAt());
        Assertions.assertEquals(alert.getMinPrice(),result.getMinimumPrice());
        Assertions.assertEquals(alert.getMaxPrice(),result.getMaxPrice());
        Assertions.assertEquals(alert.getCity(),result.getCity());
    }

}
