package com.example.alertapi.domain;

import com.example.alertapi.errors.ApplicationException;
import com.example.alertapi.infrastructure.AlertJpaRepository;
import com.example.alertapi.infrastructure.model.Alert;
import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
 class AlertServiceTest {

 @Mock private AlertJpaRepository alertJpaRepository;
 @InjectMocks private AlertServiceImpl service;

 @Test
 void whenAddAlert_whithMinimumPriceHigherThanMaximumPrice_thenReturnApplicationException(){

  //given
  final Alert alert =  Alert.builder()
          .id(1)
          .userId(1)
          .createdAt("2021-09-12T20:30:28Z")
          .alertName("alerte")
          .minimumPrice(250000)
          .maxPrice(150000)
          .city("NANTES").build();

  //when
  final ApplicationException exception = Assertions.assertThrows(
          ApplicationException.class, ()-> service.addAlert(alert));

  //then
  Assertions.assertEquals("Le prix minimum doit être inférieur au prix maximum", exception.getErrorMessage());
  verifyNoInteractions(alertJpaRepository);
 }

 @Test
 void whenAddAlert_ThenReturnAlertSaveToDataBase(){

  //given
  final Alert alert =  Alert.builder()
          .id(1)
          .userId(1)
          .createdAt("2021-09-12T20:30:28Z")
          .alertName("alerte")
          .minimumPrice(250000)
          .maxPrice(350000)
          .city("NANTES").build();
  when(alertJpaRepository.save(alert)).thenReturn(alert);

  //when
  final Alert result = service.addAlert(alert);

  //then
  Assertions.assertEquals(alert, result);
  verify(alertJpaRepository, only()).save(alert);


 }

 @Test
 void whenGetAlerts_withNoExistCustomerId_thenReturnApplicationException(){
  //given
  final int userId =1;
  when(alertJpaRepository.existsByUserId(userId)).thenReturn(false);

  //when
  final ApplicationException exception = Assertions.assertThrows(
          ApplicationException.class, ()-> service.getAlertsByUserId(userId));

  //then
  Assertions.assertEquals("Le User demandé n'existe pas", exception.getErrorMessage());
  verify(alertJpaRepository, only()).existsByUserId(userId);
 }

 @Test
 void whenGetAlerts_thenReturnAlertListByCustomer(){
  final Alert alert1 =  Alert.builder()
          .id(1)
          .userId(1)
          .createdAt("2021-09-12T20:30:28Z")
          .alertName("alerte1")
          .minimumPrice(250000)
          .maxPrice(350000)
          .city("NANTES").build();
  final Alert alert2 =  Alert.builder()
          .id(1)
          .userId(1)
          .createdAt("2021-09-12T20:30:28Z")
          .alertName("alerte2")
          .minimumPrice(250000)
          .maxPrice(350000)
          .city("RENNES").build();
  final Alert alert3 =  Alert.builder()
          .id(1)
          .userId(1)
          .createdAt("2021-09-12T20:30:28Z")
          .alertName("alerte3")
          .minimumPrice(250000)
          .maxPrice(350000)
          .city("CAEN").build();
  when(alertJpaRepository.existsByUserId(1)).thenReturn(true);
  when(alertJpaRepository.findAlertByUserId(1)).thenReturn(List.of(alert1,alert2,alert3));

  //when
  List<Alert> result = service.getAlertsByUserId(1);

  //then
  BDDAssertions.then(result).contains(alert1,alert2,alert3);
  verify(alertJpaRepository,times(1)).findAlertByUserId(1);
  verify(alertJpaRepository,times(1)).existsByUserId(1);

 }

}
