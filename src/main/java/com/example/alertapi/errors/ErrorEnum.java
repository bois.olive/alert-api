package com.example.alertapi.errors;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.zalando.problem.Status;

@Getter
@RequiredArgsConstructor
public enum ErrorEnum {
  // Commons
  TECHNICAL_ERROR(Status.INTERNAL_SERVER_ERROR),

  // Functional
  ALREADY_EXISTS_EXCEPTION(Status.BAD_REQUEST),

  DOES_NOT_EXIST_EXCEPTION(Status.NOT_FOUND),

  ILLEGAL_ARGUMENT(Status.BAD_REQUEST);

  private Status status;

  ErrorEnum(Status status) {
    this.status = status;
  }
}
