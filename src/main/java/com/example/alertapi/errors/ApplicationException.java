package com.example.alertapi.errors;

import lombok.Getter;

@Getter
public class ApplicationException extends RuntimeException {

  private static final long serialVersionUID = 1L;
  private final ErrorEnum errorEnum;
  private final String errorMessage;

  public ApplicationException(final ErrorEnum errorEnum, final String errorMessage) {
    super(errorMessage);
    this.errorEnum = errorEnum;
    this.errorMessage = errorMessage;
  }

  public ApplicationException(
      final ErrorEnum errorEnum, final String errorMessage, final Throwable throwable) {
    super(errorMessage, throwable);
    this.errorEnum = errorEnum;
    this.errorMessage = errorMessage;
  }
}
