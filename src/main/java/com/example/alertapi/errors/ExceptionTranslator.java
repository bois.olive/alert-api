package com.example.alertapi.errors;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.ProblemHandling;
import org.zalando.problem.spring.web.advice.security.SecurityAdviceTrait;

@ControllerAdvice
public class ExceptionTranslator implements ProblemHandling, SecurityAdviceTrait {
  private static final String PROBLEM_FIELD_MESSAGE = "message";
  private static final String PROBLEM_FIELD_TITLE = "errorCode";



  @ExceptionHandler
  public ResponseEntity<Problem> handle(
      final ApplicationException exception, final NativeWebRequest request) {


    final Problem problem =
        Problem.builder()
            .withStatus(exception.getErrorEnum().getStatus())
            .with(PROBLEM_FIELD_TITLE, exception.getErrorEnum().name())
            .with(PROBLEM_FIELD_MESSAGE, exception.getErrorMessage())
            .build();
    return create(exception, problem, request);
  }

  @ExceptionHandler
  public ResponseEntity<Problem> handle(final RuntimeException ex, final NativeWebRequest request) {


    final Problem problem =
        Problem.builder()
            .withStatus(Status.INTERNAL_SERVER_ERROR)
            .with(PROBLEM_FIELD_TITLE, ErrorEnum.TECHNICAL_ERROR.name())
            .with(PROBLEM_FIELD_MESSAGE, ex.getLocalizedMessage())
            .build();
    return create(ex, problem, request);
  }
}
