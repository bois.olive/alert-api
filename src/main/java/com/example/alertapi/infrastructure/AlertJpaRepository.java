package com.example.alertapi.infrastructure;

import com.example.alertapi.infrastructure.model.Alert;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlertJpaRepository extends JpaRepository<Alert, Integer> {

    List<Alert>findAlertByUserId(final int userId);

    boolean existsByUserId(final int userId);
}
