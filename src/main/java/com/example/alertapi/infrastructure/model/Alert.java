package com.example.alertapi.infrastructure.model;

import lombok.*;

import javax.persistence.*;

@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@Entity
@Table(catalog = "Figaro", schema = "alert", name = "Alert")
public class Alert {

    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "UserId", nullable = false)
    private Integer userId;

    @Column(name = "CreatedAt", nullable = false, length = 30)
    private String createdAt;

    @Column(name = "AlertName", nullable = false , length = 30)
    private String alertName;

    @Column(name = "MinPrice")
    private Integer minimumPrice;

    @Column(name = "MaxPrice")
    private Integer maxPrice;

    @Column(name = "City", nullable = false, length = 50)
    private String city;
}
