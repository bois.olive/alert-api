package com.example.alertapi.domain;

import com.example.alertapi.errors.ApplicationException;
import com.example.alertapi.errors.ErrorEnum;
import com.example.alertapi.infrastructure.AlertJpaRepository;
import com.example.alertapi.infrastructure.model.Alert;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class AlertServiceImpl implements AlertService{

    private final AlertJpaRepository alertJpaRepository;

    @Override
    public Alert addAlert(final Alert alert) {

        if (alert.getMaxPrice() < alert.getMinimumPrice()){
            throw new ApplicationException(ErrorEnum.ILLEGAL_ARGUMENT, "Le prix minimum doit être inférieur au prix maximum");
        }

        return alertJpaRepository.save(alert);
    }

    @Override
    public List<Alert> getAlertsByUserId(final int userId) {

        if (!alertJpaRepository.existsByUserId(userId)){
            throw new ApplicationException(ErrorEnum.DOES_NOT_EXIST_EXCEPTION, "Le User demandé n'existe pas");
        }
        return alertJpaRepository.findAlertByUserId(userId);
    }
}
