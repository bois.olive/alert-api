package com.example.alertapi.domain;

import com.example.alertapi.infrastructure.model.Alert;
import com.example.alertapi.web.model.AlertDto;

import java.util.List;

public interface AlertService {

    Alert addAlert(Alert alert);

    List<Alert> getAlertsByUserId(final int userId);
}
