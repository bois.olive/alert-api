package com.example.alertapi.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

@Configuration
public class ApplicationWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

  private final SecurityProblemSupport problemSupport;

  private final RequestMatcher noAuthMatcher;

  private final boolean h2ConsoleEnabled;

  public ApplicationWebSecurityConfigurerAdapter(
      final SecurityProblemSupport problemSupport,
      final RequestMatcher noAuthMatcher,
      @Value("${spring.h2.console.enabled:false}") final boolean h2ConsoleEnabled) {
    this.problemSupport = problemSupport;
    this.noAuthMatcher = noAuthMatcher;
    this.h2ConsoleEnabled = h2ConsoleEnabled;
  }

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    if (h2ConsoleEnabled) {
      http.headers().frameOptions().disable();
    }
    http.cors()
        .and()
        .csrf()
        .disable()
        .exceptionHandling()
        .accessDeniedHandler(problemSupport)
        .and()
        .authorizeRequests()
        .requestMatchers(noAuthMatcher)
        .permitAll()
        .anyRequest()
        .authenticated()
        .and()
        .httpBasic(Customizer.withDefaults())
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }
}
