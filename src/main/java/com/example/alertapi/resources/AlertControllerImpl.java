package com.example.alertapi.resources;

import com.example.alertapi.domain.AlertService;
import com.example.alertapi.resources.mapper.AlertMapper;
import com.example.alertapi.web.api.AlertApiDelegate;
import com.example.alertapi.web.model.AlertDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class AlertControllerImpl implements AlertApiDelegate {

    private final AlertMapper alertMapper;
    private final AlertService alertService;


    @Override
    public ResponseEntity<AlertDto> addAlert(AlertDto alertDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(alertMapper.mapFrom(alertService.addAlert(alertMapper.mapTo(alertDto))));
    }

    @Override
    public ResponseEntity<List<AlertDto>> getAlertsByCustomer(Integer userId) {
        return ResponseEntity.ok(alertService.getAlertsByUserId(userId).stream()
                .map(alertMapper::mapFrom)
                .collect(Collectors.toUnmodifiableList()));
    }
}
