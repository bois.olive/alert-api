package com.example.alertapi.resources.mapper;

import com.example.alertapi.infrastructure.model.Alert;
import com.example.alertapi.web.model.AlertDto;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Optional;

@Component
public class AlertMapper {

    public AlertDto mapFrom(final Alert alert) {
        return Optional.ofNullable(alert).map(this::mapFromAlert).orElse(null);
    }

    private AlertDto mapFromAlert(final Alert alert) {
        return new AlertDto()
                .id(alert.getId())
                .userId(alert.getUserId())
                .createdAt(Instant.parse(alert.getCreatedAt()))
                .alertName(alert.getAlertName())
                .minPrice(alert.getMinimumPrice())
                .maxPrice(alert.getMaxPrice())
                .city(alert.getCity());
    }

    public Alert mapTo(final AlertDto alertDto) {
        return Optional.ofNullable(alertDto).map(this::mapToAlert).orElse(null);
    }

    private Alert mapToAlert(final AlertDto alertDto) {
        return Alert.builder()
                .id(alertDto.getId())
                .userId(alertDto.getUserId())
                .createdAt(alertDto.getCreatedAt().toString())
                .alertName(alertDto.getAlertName())
                .minimumPrice(alertDto.getMinPrice())
                .maxPrice(alertDto.getMaxPrice())
                .city(alertDto.getCity()).build();
    }
}
