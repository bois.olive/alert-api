CREATE TABLE alert.Alert
(
    Id INT IDENTITY (1,1) NOT NULL ,
    UserId INT NOT NULL,
    CreatedAt VARCHAR(30) NOT NULL,
    AlertName VARCHAR (30) NOT NULL ,
    MinPrice INT ,
    MaxPrice INT ,
    City VARCHAR(50) NOT NULL ,
    CONSTRAINT PK_Alert PRIMARY KEY(Id)
)