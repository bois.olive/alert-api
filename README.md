Afin de la mettre en place, tu dois disposer d'un JDK 11.0.12, ensuite il faut tirer les dépendances et faire
un build de l'application pour générer le contrat d'interfaces Open-Api.

Le projet est donc codé en Api first et tu trouveras le descriptif de l'Api ici 
src/main/resources/openapi/alert-api.yaml, ensuite un contrat d'interface est généré 
et il est implémenté dans les classes JAVA.

Pour le Run de l'application ne pas oublié de mettre le profil dev dans la conf du run.

J'ai utilisé une base de données en mémoire avec un liquibase qui crée la base de données et la versionne.

Il y 'a déjà 9 Alertes dans la BDD avec les users id 1, 2 et 3.

Tu peux consulter la BDD à cette URL une fois le projet lancé : http://localhost:9093/h2-console, 
une fois sur la page de connexion de la BDD tu arrives sur ce masque qui doit être 
renseigné à l'identique de ce que tu vois ci dessous.

<img src="image.png">


Pour tester l'api 2 solutions soit le swagger Accessible à cette URL : http://localhost:9093/swagger-ui.html
une popup d'identification va s'afficher login : user, password: password

soit via postman qu'il faut configurer pour l' Authorization type à BasicAuth Username : user et Password : password

2 endpoints sont disponibles

-  un Post : localhost:9093/v1/alert
   dans ce cas dans le body de postman tu configure sur raw en format JSON et tu peux envoyer un objet de ce type :

    {
        "userId": 1,
        "createdAt": "2021-10-12T20:30:28Z",
        "alertName": "Alerte Maison",
        "minPrice": 125000,
        "maxPrice": 50000,
        "city": "NANTES"
    }

et un get :

localhost:9093/v1/alert/{userId}  ex: 1